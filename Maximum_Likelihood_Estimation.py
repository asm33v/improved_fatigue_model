# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 18:46:10 2024

@author: asm33v
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm, kstest

# Morning session data
morning_rt = np.array([
    0.2154839, 0.2534544, 0.2125694, 0.2620212, 0.2566101, 0.3305923, 0.2950737,
    0.2319023, 0.2613174, 0.2780258, 0.2635109, 0.3208839, 0.2313072, 0.2361977,
    0.3636772, 0.2817337, 0.2616608, 0.279956, 0.2493546, 0.2079117, 0.2090561,
    0.2123596, 0.2507583, 0.2304871, 0.3066208, 0.4969589, 0.2629883, 0.3637955,
    0.2890655, 0.278865, 0.2643158, 0.2458527, 0.2293579, 0.3293182, 0.3066131,
    0.2700683, 0.2317535, 0.3383514, 0.2354767, 0.3138611, 0.3076813, 0.3141052,
    0.374057, 0.2493164, 0.2230103, 0.2469971, 0.2089417
])

# Fit morning data to a normal distribution
mu_morning, std_morning = norm.fit(morning_rt)
# Night session data 
night_rt = np.array([
    0.2954804, 0.3821108, 0.2282249, 0.2363922, 0.2291863, 0.4043762, 0.2313683,
    0.2455246, 0.3422462, 0.2486031, 0.268222, 0.3472931, 0.355365, 0.3499824,
    0.2797233, 0.3141739, 0.2836563, 0.1954224, 0.2353317, 0.3905899, 0.2518417,
    0.2724869, 0.3285171, 0.2449066, 0.5983536, 0.3175232, 0.2691757, 0.2968246,
    0.268985, 0.3456299, 0.2641632, 0.3147308, 0.2680084, 0.2789337, 0.329715,
    0.3475067, 0.3359863, 0.2707397, 0.3299591, 0.3416626, 0.3031494, 0.3336365,
    0.3275482, 0.2744934
])

# Fit the night data to a normal distribution
mu_night, std_night = norm.fit(night_rt)

# Estimate afternoon session parameters by interpolating between morning and night parameters
mu_afternoon = (mu_morning + mu_night) / 2
std_afternoon = (std_morning + std_night) / 2

# Predict afternoon reaction times using the estimated parameters
predicted_afternoon_rt = np.random.normal(mu_afternoon, std_afternoon, len(night_rt))

# Plot the predicted afternoon reaction times
plt.figure(figsize=(10,6))
plt.plot(predicted_afternoon_rt, label='Predicted Afternoon RT', marker='x')
plt.title('Predicted Reaction Times (Afternoon Session)')
plt.xlabel('Index')
plt.ylabel('Reaction Time (s)')
plt.legend()
plt.grid(True)
plt.show()

# Display predicted afternoon RT values
predicted_afternoon_rt


# Visualizing the distribution fits for morning and night data along with the interpolated afternoon distribution


# Perform Kolmogorov-Smirnov goodness-of-fit tests
ks_test_morning = kstest(morning_rt, 'norm', args=(mu_morning, std_morning))
ks_test_night = kstest(night_rt, 'norm', args=(mu_night, std_night))
ks_test_afternoon = kstest(predicted_afternoon_rt, 'norm', args=(mu_afternoon, std_afternoon))

# Generate x-values for plotting the PDFs
x_values = np.linspace(min(min(morning_rt), min(night_rt)), max(max(morning_rt), max(night_rt)), 1000)
morning_pdf = norm.pdf(x_values, mu_morning, std_morning)
night_pdf = norm.pdf(x_values, mu_night, std_night)
afternoon_pdf = norm.pdf(x_values, mu_afternoon, std_afternoon)

# Plot the PDFs for morning, night, and predicted afternoon
plt.figure(figsize=(10, 6))
plt.plot(x_values, morning_pdf, label='Morning RT Distribution', color='yellow')
plt.plot(x_values, night_pdf, label='Night RT Distribution', color='purple')
plt.plot(x_values, afternoon_pdf, label='Predicted Afternoon RT Distribution', color='red', linestyle='--')
plt.title('Reaction Time Distributions: Morning, Night, and Predicted Afternoon')
plt.xlabel('Reaction Time (s)')
plt.ylabel('Probability Density')
plt.legend()
plt.grid(True)
plt.show()

# Display the results of the Kolmogorov-Smirnov tests
print("Kolmogorov-Smirnov Test Results:")
print(f"Morning Data: Statistic = {ks_test_morning.statistic}, p-value = {ks_test_morning.pvalue}")
print(f"Night Data: Statistic = {ks_test_night.statistic}, p-value = {ks_test_night.pvalue}")
print(f"Predicted Afternoon Data: Statistic = {ks_test_afternoon.statistic}, p-value = {ks_test_afternoon.pvalue}")