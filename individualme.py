# -*- coding: utf-8 -*-
"""
Created on Wed Jun  5 14:25:37 2024

@author: asm33v
"""

import pandas as pd

df = pd.read_excel("C:/Users/asm33v/Desktop/me.xlsx")

#print(df.head())

#print(df.columns)

#print(df['Session start'])

df['Timestamp'] = pd.to_datetime(df['Session start'])
#print(df['Timestamp'])

#df['Timestamp'] = pd.Timestamp().time(df['Timestamp'])
df['Timestamp'] = df['Timestamp'].apply(lambda x: x.time())

subject_ids = set(df['Subject Id'])
#print(len(subject_ids))
sub_dict = dict()

for subject_id in subject_ids:
    sub_dict[subject_id] = df.loc[df['Subject Id'] == subject_id]

print(sub_dict[101])



#print(sub_dict[102].to_list())

#separated_df = pd.DataFrame()
#for key in sub_dict.keys():
#    separated_df[key] = sub_dict[key]
    
with pd.ExcelWriter("C:/Users/asm33v/Desktop/separated_me.xlsx") as writer:
   
    # use to_excel function and specify the sheet_name and index 
    # to store the dataframe in specified sheet
    for key in sub_dict.keys():
        sub_dict[key].to_excel(writer, sheet_name=str(key), index=False)
    #data_frame2.to_excel(writer, sheet_name="Vegetables", index=False)
    #data_frame3.to_excel(writer, sheet_name="Baked Items", index=False)
#print(sub_dict)


#print(df['Time'])

#print(df.columns)

#print(df.head())