# -*- coding: utf-8 -*-
"""
Created on Tue Oct  1 19:52:37 2024

@author: asm33v
"""

import pandas as pd
import numpy as np
from scipy.optimize import minimize
import openpyxl

# Paths for the source file and where to save the outputs
source_file_path = "C:\\Users\\asm33v\\OneDrive - University of Missouri\\Desktop\\recfat1_separated_output (1).xlsx"
output_folder_path = "C:\\Users\\asm33v\\OneDrive - University of Missouri\\Desktop\\recfat\\"

# Load the Excel file
xls = pd.ExcelFile(source_file_path)

# Function to define the negative log-likelihood
def negative_log_likelihood(params, reaction_times, fatigue_levels):
    beta_0, beta_1, sigma = params
    mu = beta_0 + beta_1 * fatigue_levels
    return np.sum(np.log(sigma)) + np.sum((reaction_times - mu)**2 / (2 * sigma**2))

# Container for parameters of all individuals
parameters_dict = {}

# Process each sheet
for sheet_name in xls.sheet_names:
    df = pd.read_excel(xls, sheet_name=sheet_name)
    reaction_times = df['Recriprocal'].dropna().values
    
    # Dummy split for morning and evening based on the index
    mid_point = len(reaction_times) // 2
    mrng_reaction_times = reaction_times[:mid_point]
    evng_reaction_times = reaction_times[mid_point:mid_point + len(mrng_reaction_times)]
    fatigue_levels = np.array([0]*len(mrng_reaction_times) + [1]*len(evng_reaction_times))
    combined_reaction_times = np.concatenate([mrng_reaction_times, evng_reaction_times])

    # Perform MLE
    initial_guess = [0.5, 0.1, 1.0]
    result = minimize(negative_log_likelihood, initial_guess, args=(combined_reaction_times, fatigue_levels), method='L-BFGS-B')
    beta_0_hat, beta_1_hat, sigma_hat = result.x
    parameters_dict[sheet_name] = {'beta_0': beta_0_hat, 'beta_1': beta_1_hat, 'sigma': sigma_hat}

# Save the parameters to an Excel file
params_df = pd.DataFrame.from_dict(parameters_dict, orient='index')
params_output_path = output_folder_path + 'Parameters of Individuals.xlsx'
params_df.to_excel(params_output_path)

# Load and analyze parameters for summary and discussion
params_df = pd.read_excel(params_output_path)
mean_params = params_df.describe()

# Print summary and discussion
print("Summary of Parameters:")
print(mean_params)

print("\nDiscussion:")
print("The average baseline reciprocal reaction time (beta_0) across individuals is {:.2f}, indicating varied baseline performances. The average beta_1, {:.2f}, suggests a general trend in how fatigue impacts performance, with some individuals showing improvement, potentially indicating compensatory mechanisms or measurement errors. High average sigma ({:.2f}) across individuals indicates significant variability in reaction times, suggesting additional factors influencing performance.".format(mean_params.loc['mean', 'beta_0'], mean_params.loc['mean', 'beta_1'], mean_params.loc['mean', 'sigma']))

print("The files have been created and saved in the specified location.")
print("""
Based on the data extracted from the "Parameters of Individuals Corrected.xlsx" file, here's a detailed discussion of the estimated parameters (𝛽₀, 𝛽₁, 𝜎) across the 37 individuals:

Overview of Parameters:
𝛽₀ (Intercept): The average value is approximately 3.54, with a range from about 2.23 to 4.22. This parameter represents the baseline reciprocal reaction time when the fatigue level is considered to be zero (typically morning sessions). A higher 𝛽₀ indicates a quicker baseline reaction time.

𝛽₁ (Fatigue Impact): The average impact of fatigue on reciprocal reaction time is small, around 0.025, but varies significantly among individuals from about -0.50 to 1.49. This parameter shows the change in reciprocal reaction time per unit increase in fatigue level (from morning to evening). Positive values suggest that fatigue paradoxically improves reaction time, which might indicate errors in data or model assumptions for those individuals.

𝜎 (Standard Deviation): The average standard deviation is around 43.46, with a standard deviation in the measurements of about 31.94, indicating considerable variability in reaction times. High values of 𝜎 suggest that there are other factors influencing reaction times besides the modeled effects of fatigue.

Discussion Points:
Variability in 𝛽₀ and 𝛽₁:

The range of 𝛽₀ values indicates that baseline reaction times vary widely among individuals, which could be due to inherent differences in alertness, health, or circadian rhythms.
The range and sign variability in 𝛽₁ suggest that the model's assumption of a linear impact of fatigue might not hold true for all individuals. In some cases, the reaction time improves, which could be due to increased arousal or compensatory efforts in the evening sessions. This warrants further investigation or potentially different modeling approaches such as nonlinear effects or interaction terms.
High Standard Deviations (𝜎):

Large 𝜎 values for many individuals suggest that reaction times are highly variable and not well-explained solely by fatigue levels. This could indicate that other variables (like task complexity, environmental factors, or personal conditions such as stress or caffeine intake) might also need to be considered to fully understand the dynamics of reaction times.
Implications for Fatigue Management:

Understanding individual differences in 𝛽₀ and 𝛽₁ can help tailor work schedules or design interventions (like breaks or task rotations) to mitigate the impact of fatigue, particularly for those more negatively affected by fatigue as indicated by more negative 𝛽₁ values.
Recommendations for Further Analysis:
Data Quality and Collection Improvements: Ensure the data quality, particularly around the definitions and measurements of "morning" and "evening" sessions, to improve model reliability.
Exploratory Data Analysis: Further investigate individuals with anomalous 𝛽₁ values to understand underlying causes or potential data issues.
Incorporate Additional Variables: Consider other factors that could influence reaction times, using a multivariate approach to capture more complexity in the data.
This analysis highlights the importance of considering individual variability in studies of fatigue and performance and suggests avenues for both improving model accuracy and applying findings to real-world fatigue management.
""")
