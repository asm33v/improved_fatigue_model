#-----------------------------------------------------------------------------
# Library Imports
#-----------------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt

from typing import Optional
#-----------------------------------------------------------------------------
# Class: 	    HumanRTeModel
# 
# Description: 	Object that runs the human response time simulation.
#
#-----------------------------------------------------------------------------
class HumanRTModel:
    
    #private member variables
    _rt_mean: float = None #mean of response time
    _rt_stdv: float = None #standard deviation of response time
    _rt_mean_delta: float = None #change in mean of response time
    _rt_stdv_delta: float = None #change in standard deviation of response time
    _cap_factor: float = None #capability of subject
    _instances: int = None #number of iterations
    _rt_list: list[float] = [] #list of response times

    #-------------------------------------------------------------------------
    # Function: 	*Default Constructor
    # 
    # Description:  Default constructor for Simulation.
    #
    # Parameters:   rt_mean - mean of response time
    #               rt_stdv - standard deviation of response time
    #               rt_mean_delta - change in mean of response time
    #               rt_stdv_delta - change in standard deviation of response time
    #               cap_factor - capability of subject
    #               instances - number of iterations
    #
    # Returns:      None
    #
    #-------------------------------------------------------------------------
    def __init__(self, rt_mean: float, rt_stdv: float, rt_mean_delta: float, \
                 rt_stdv_delta: float, cap_factor: float, instances: int, complexity: Optional[int] = None,experience: Optional[int] = None):
        
        self._rt_mean = rt_mean
        self._rt_stdv = rt_stdv
        self._rt_mean_delta = rt_mean_delta
        self._rt_stdv_delta = rt_stdv_delta
        self._cap_factor = cap_factor
        self._instances = instances
        self._complexity = complexity
        self._experience = experience
        self._generate_samples()
        self._plot_rt_data()
        
        return

    #-------------------------------------------------------------------------
    # Function: 	*Default Constructor
    # 
    # Description:  Default constructor for Simulation.
    #
    # Parameters:   None
    #
    # Returns:      None
    #
    #-------------------------------------------------------------------------
    def _generate_samples(self):
        sample: np.ndarray = None
        response_time: float = None
        
        for index in range(self._instances):
            sample = np.random.normal(self._rt_mean, self._rt_stdv, 1)
            
            if (len(sample) > 0):
                response_time = sample.item()
            
            self._rt_list.append(response_time)
            
            self._rt_mean = self._rt_mean + (self._rt_mean_delta * self._cap_factor)
            self._rt_stdv = self._rt_stdv + (self._rt_stdv_delta * self._cap_factor)
            
        #print(self._rt_list)
        
        return
    
    #-------------------------------------------------------------------------
    # Function: 	*Default Constructor
    # 
    # Description:  Default constructor for Simulation.
    #
    # Parameters:   one
    #
    # Returns:      None
    #
    #-------------------------------------------------------------------------    
    def _plot_rt_data(self):
        scale: float = 0.5
        width: float = scale * 16
        height: float = scale * 9
        instance_values: list = []
        
        #gather data
        for index in range(len(self._rt_list)):
            instance_values.append(index)
            
        #plot data
        plt.figure(figsize=(width, height)) 
        
        plt.plot(instance_values, self._rt_list, c = "purple", marker = "o",\
                 label = "Response Time")
        
        plt.xlim((0, instance_values[-1]))
        #plt.ylim((0, 0))
        plt.title("Simulated Response Time(s)")
        plt.xlabel("Instance")
        plt.ylabel("Response Time")
        plt.legend(loc = "upper left")
        
        return