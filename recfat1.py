import os
import pandas as pd
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import math
import numpy as np

def sanitize_filename(filename):
    """Replace invalid characters in filenames."""
    return filename.replace(':', '-').replace('/', '-').replace('\\', '-')

def plot_reaction_times(data, subject_id, session, output_directory):
    data['Timestamp'] = pd.to_datetime(data['Timestamp'], format='%H:%M:%S')
    data['ReactionTime'] = data['ReactionTime'].astype(float)
    data['Recriprocal'] = 1 / data['ReactionTime']
    mean_reaction_time = data['Recriprocal'].mean()

    # Plotting
    plt.figure(figsize=(12, 6))
    plt.scatter(data['Timestamp'], data['Recriprocal'], color='blue')
    plt.axhline(y=mean_reaction_time, color='gray', linestyle='dotted', label=f'Mean: {mean_reaction_time:.2f}')
    plt.title(f'Reciprocal vs. Timestamp for {subject_id} at {session}')
    plt.xlabel('Timestamp')
    plt.ylabel('Reciprocal')
    plt.grid(True)

    # Format the x-axis to display timestamps properly
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
    plt.gca().xaxis.set_major_locator(mdates.AutoDateLocator())
    plt.ylim(bottom=0.0, top=10)
    
    # Automatically adjust the date labels for better clarity
    plt.gcf().autofmt_xdate()
    
    plt.legend()

    # Sanitize session name for file saving
    session_safe = sanitize_filename(session)

    # Save plot to a file
    plot_file_path = os.path.join(output_directory, f"{subject_id}_{session_safe}.png")
    plt.savefig(plot_file_path)
    plt.close()

    return plot_file_path

df_meta = {}
df_rtimetable = {}
input_directory = "C:\\Users\\asm33v\\OneDrive - University of Missouri\\Desktop\\Export_2024-04-23-1138\\PVT"
output_directory = "C:\\Users\\asm33v\\Desktop\\plots"
os.makedirs(output_directory, exist_ok=True)

for file in os.listdir(input_directory):
    if file.endswith(".csv"):
        file_path = os.path.join(input_directory, file)
        df = pd.read_csv(file_path, nrows=1)
        individual = df['Subject Id'][0][-3:]

        df1 = pd.read_csv(file_path)
        index = int(np.where(df1 == 'Timestamp')[0])
        new_header = df1.iloc[index]  # grab the first row for the header
        df1 = df1[index + 1:]  # take the data less the header row
        df1.columns = new_header  # set the header row as the df header

        df = df[['Session start', 'Session end', 'Mean RT (ms)', 'Mean RT Reciprocal (1/s)', 
                 'Median RT (ms)', 'Minimum RT (ms)', 'Maximum RT (ms)', 'Std Dev RT (ms)']]

        df['Session start'] = df['Session start'].str.replace("T0 ", 'T0')
        start_time = pd.to_datetime(df['Session start'])

        df1 = df1[['Timestamp', 'ReactionTime']]
        if df1.size == 0:
            continue

        df1 = df1.dropna(how='any', axis=0)
        df1 = df1[df1['ReactionTime'] != '0']
        df1['Timestamp'] = df1['Timestamp'].apply(lambda x: start_time + timedelta(seconds=math.ceil(float(x))))
        df1['Timestamp'] = df1['Timestamp'].dt.time
        df1['ReactionTime'] = df1['ReactionTime'].astype(float)

        if individual in df_meta:
            df_meta[individual][len(df_meta[individual].keys())] = df
            df_rtimetable[individual][len(df_rtimetable[individual].keys())] = df1
        else:
            df_meta[individual] = {0: df}
            df_rtimetable[individual] = {0: df1}

combined_dataframes = {}
for key in df_meta.keys():
    combined_list = []
    for i in range(len(df_meta[key])): 
        df_meta_key_i = df_meta[key][i]
        df_rtimetable_key_i = df_rtimetable[key][i]

        # Generate plot and get the plot file path
        plot_file_path = plot_reaction_times(df_rtimetable_key_i, key, df_meta_key_i['Session start'][0], output_directory)

        # Convert df_meta to the format of df_rtimetable
        meta_data_as_rows = pd.DataFrame({'Timestamp': ['Session start', 'Session end', 'Mean RT (ms)', 'Mean RT Reciprocal (1/s)', 
                                                       'Median RT (ms)', 'Minimum RT (ms)', 'Maximum RT (ms)', 'Std Dev RT (ms)'],
                                          'ReactionTime': [
                                              df_meta_key_i['Session start'][0],
                                              df_meta_key_i['Session end'][0],
                                              df_meta_key_i['Mean RT (ms)'][0],
                                              df_meta_key_i['Mean RT Reciprocal (1/s)'][0],
                                              df_meta_key_i['Median RT (ms)'][0],
                                              df_meta_key_i['Minimum RT (ms)'][0],
                                              df_meta_key_i['Maximum RT (ms)'][0],
                                              df_meta_key_i['Std Dev RT (ms)'][0],
                                          ]})
        df_rtimetable_key_i['Timestamp'] = df_rtimetable_key_i['Timestamp'].dt.time
        # Concatenate df_rtimetable and df_meta_as_rows vertically
        df_combined = pd.concat([meta_data_as_rows, df_rtimetable_key_i], ignore_index=True)
        combined_list.append(df_combined)

        # Store the combined DataFrame in a new dictionary for each key
    combined_dataframes[key] = pd.concat(combined_list, ignore_index=True)

# Saving data and plots to an Excel file
with pd.ExcelWriter("C:/Users/asm33v/Desktop/recfat1_separated_output.xlsx", engine='xlsxwriter') as writer:
    for key in combined_dataframes.keys():
        combined_dataframes[key].to_excel(writer, sheet_name=key, index=False)
        
        # Insert plot beside each session in the corresponding sheet
        workbook  = writer.book
        worksheet = writer.sheets[key]
        
        for i in range(len(df_meta[key])):
            df_meta_key_i = df_meta[key][i]
            df_rtimetable_key_i = df_rtimetable[key][i]

            # Place the image beside the session data (assuming the session data ends in column 'F')
            # So we start placing the image in column 'H' beside the session data
            row_position = 2 + i * (len(df_rtimetable_key_i) + 10)  # Adjust spacing between sessions
            col_position = 'H'
            cell_position = f'{col_position}{row_position}'
            
            # Generate plot file path
            plot_file_path = os.path.join(output_directory, f"{key}_{sanitize_filename(df_meta_key_i['Session start'][0])}.png")
            
            # Insert image beside the session data in the Excel sheet
            worksheet.insert_image(cell_position, plot_file_path)
