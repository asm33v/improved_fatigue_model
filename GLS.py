# -*- coding: utf-8 -*-
"""
Created on Wed Jun 12 13:56:29 2024

@author: asm33v
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Jun 12 13:42:22 2024

@author: asm33v
"""

import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm

# Define datasets
# Define your datasets
datasets = {
    'DF120': np.array([13, 24.6, 16, 9.6, 12.4, -5.6, -25.4, -19.4, -7.5]),
    'DF122': np.array([-37.7, -2.2, 2.9, -35.2, 6.7, -3.2, 4.3, -31.1, -32.2]),
    'DF116': np.array([9, 90, -14.4, -31.1, -16.4, 5.9, -40.5, -10.7, -2.3]),
    'DF128': np.array([104.8, 11, -13.7, 85.3, -7.1, -121.5, -121.2, 10.6, 111.2]),
    'DF131': np.array([66.4, -4.6, -12.3, -2.7, -5.8, -6.3, 4.8, 3.1, -3.1]),
    'DF132': np.array([-2.3, 89.5, 4.5, 140.2, 44.1, 62.1, -85.6, -6.2, 19.9]),
    'DF101': np.array([-29.9, -3.9, -20.1, -15.4, -1.9, 170.8, -26.8, -9.6, -27.8]),
    'DF109': np.array([-28.2, 3.4, 30, 15.1, 20.3, 10.5, 32.1, 3, 10.6]),
    'DF127': np.array([51.2, 8.9, -14.3, 9.6, -26.6, -6.5, 34.1, 35.2, 4.7]),
    'DF125': np.array([103.6, 54.4, 49.8, 24.9, 0, -19.5, 2, -7.7, 15.9]),
    'DF124': np.array([29.4, -47.5, 23.1, 37, -44.4, 34.4, 34.9, -9.6, -20]),
    'DF121': np.array([-25, 74.9, -12.8, -71, 0.8, 31, -33.9, -87.3, 21.8]),
    'DF110': np.array([-6.1, -3, -23.1, 0.3, -34.8, -16.6, -9.4, -22, -29.2])
}

# Iterate over each dataset and perform GLS
plt.figure(figsize=(15, 20))
plot_index = 1

# Iterate over each dataset
for name, data in datasets.items():
    days = np.arange(1, len(data) + 1)
    # Adding polynomial terms
    X = np.column_stack((np.ones(len(days)), days, np.power(days, 2), np.power(days, 3), np.power(days, 4)))

    # Fit a GLS model
    model = sm.GLS(data, X)
    results = model.fit()

    # Accessing the log-likelihood
    log_likelihood = results.llf
    print(f"Log-Likelihood for {name}: {log_likelihood}")

    # Calculate Adjusted R^2
    r_squared_adj = results.rsquared_adj

    # Plotting
    ax = plt.subplot(5, 3, plot_index)
    plt.scatter(days, data, label='Data')
    plt.plot(days, results.fittedvalues, 'r-', label=f'GLS Fit, Adj R²={r_squared_adj:.2f}')
    plt.title(name)
    plt.xlabel('Days')
    plt.ylabel('Reaction Diff')
    plt.legend()

    plot_index += 1

plt.tight_layout()
plt.show()

# Optionally print out the summary of the last fit
print(results.summary())