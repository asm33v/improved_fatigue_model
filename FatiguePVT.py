# -*- coding: utf-8 -*-
"""
Created on Mon May 13 11:13:31 2024

@author: asm33v
"""

import numpy as np
import math
import pandas as pd


class PVTModel:
    def __init__(self, factors):
        self.factors = factors  # Individual factors for morning, afternoon, and night
        self.std_dev = 0.05  # Standard deviation for reaction time
        self.days = 30

    def _fatigue(self, cap_factor: float, avg_util: float, man_asm: int):
        fat_acc = (1 / cap_factor) * (math.exp(avg_util * man_asm) - 1)
        return fat_acc

    def _response_time(self, fat_lvl: float, cap_factor: float, res_origin: float):
        mean = res_origin + ((1 / cap_factor) * math.log(fat_lvl + 1))
        rsp_pre = np.random.normal(mean, self.std_dev, 1).item()
        rsp = max(rsp_pre, 0)  # Ensure response time is not negative
        return rsp

    def simulate(self):
        results = pd.DataFrame(
            columns=['Individual', 'Day', 'TimeOfDay', 'ReactionTime', 'Fatigue'])
        time_of_day_modifiers = {
            'Morning': 1.0,
            'Afternoon': 0.9,
            'Night': 0.85
        }

        for individual, factors in enumerate(self.factors, start=1):
            for day in range(1, self.days + 1):
                for time_of_day, modifier in time_of_day_modifiers.items():
                    cap_factor = factors[time_of_day]
                    # Base reaction time adjusted for time of day
                    reaction_time_base = 0.2433 * modifier
                    # Arbitrary manual asm for example
                    fatigue = self._fatigue(cap_factor, reaction_time_base, 2)
                    reaction_time = self._response_time(
                        fatigue, cap_factor, reaction_time_base)

                    new_row = pd.DataFrame({
                        'Individual': [f'Person {individual}'],
                        'Day': [day],
                        'TimeOfDay': [time_of_day],
                        'ReactionTime': [reaction_time],
                        'Fatigue': [fatigue]
                    })
                    results = pd.concat([results, new_row], ignore_index=True)

        return results

#---------------------------------------------------------------------------------------------------------------------
# Factors for each individual and time of day
factors = [
    {'Morning': 1.0, 'Afternoon': 0.95, 'Night': 0.9},  # Individual 1
    {'Morning': 0.9, 'Afternoon': 0.85, 'Night': 0.8},  # Individual 2
    {'Morning': 0.85, 'Afternoon': 0.8, 'Night': 0.75}  # Individual 3
]

pvt_model = PVTModel(factors)
result_data = pvt_model.simulate()
print(result_data)

# Plotting the fatigue with specified colors for each time of day for each individual
plt.figure(figsize=(14, 8))

# Filter and plot for each individual and time of day with specified colors
colors = {'Morning': 'yellow', 'Afternoon': 'red', 'Night': 'blue'}
for individual in range(1, 4):
    for time_of_day, color in colors.items():
        subset = result_data[(result_data['Individual'] == f'Person {individual}') & (result_data['TimeOfDay'] == time_of_day)]
        plt.plot(subset['Day'], subset['Fatigue'], label=f'Person {individual} {time_of_day}', color=color)

plt.title('Fatigue Levels Over 30 Days by Individual and Time of Day')
plt.xlabel('Day')
plt.ylabel('Fatigue Level')
plt.legend(title='Individual / Time of Day')
plt.grid(True)
plt.show()
#--------------------------------------------------------------------------------------------------------------------
# Calculate the difference in fatigue levels between each pair of individuals
plt.figure(figsize=(14, 8))

# Individual pairs and their comparisons
pairs = [('Person 1', 'Person 2'), ('Person 1', 'Person 3'), ('Person 2', 'Person 3')]

for pair in pairs:
    for time_of_day, color in colors.items():
        data1 = result_data[(result_data['Individual'] == pair[0]) & (result_data['TimeOfDay'] == time_of_day)]
        data2 = result_data[(result_data['Individual'] == pair[1]) & (result_data['TimeOfDay'] == time_of_day)]
        fatigue_diff = data1['Fatigue'].values - data2['Fatigue'].values
        plt.plot(data1['Day'], fatigue_diff, label=f'{pair[0]} vs {pair[1]} {time_of_day}', color=color)

plt.title('Difference in Fatigue Levels Between Individuals Over 30 Days')
plt.xlabel('Day')
plt.ylabel('Difference in Fatigue Level')
plt.legend(title='Individual Comparison / Time of Day')
plt.grid(True)
plt.show()
#----------------------------------------------------------------------------------------------------------------------
# Convert the 'Day' and 'Fatigue' columns to numeric types if they are not already
result_data['Day'] = pd.to_numeric(result_data['Day'], errors='coerce')
result_data['Fatigue'] = pd.to_numeric(result_data['Fatigue'], errors='coerce')

# Check for any non-numeric entries that could be causing issues
result_data.info()

# Attempt plotting again with type-checked data
plt.figure(figsize=(14, 8))

# Plot each individual's fatigue with a regression line to show the trend
for individual in range(1, 4):
    for time_of_day, color in colors.items():
        subset = result_data[(result_data['Individual'] == f'Person {individual}') & (result_data['TimeOfDay'] == time_of_day)]
        sns.regplot(x='Day', y='Fatigue', data=subset, scatter_kws={'color': color}, line_kws={'color': color}, label=f'Person {individual} {time_of_day}')

plt.title('Trend Lines of Fatigue Levels by Individual and Time of Day')
plt.xlabel('Day')
plt.ylabel('Fatigue Level')
plt.legend(title='Individual / Time of Day')
plt.grid(True)
plt.show()
#--------------------------------------------------------------------------------------------------------------------------
# Plotting with a focus on fluctuations and frequencies using a scatter plot and a smoother line to highlight trends
plt.figure(figsize=(18, 10))

# Use a lowess smoother for a clearer view of fluctuations and frequencies
import statsmodels.api as sm

# Individual and color settings for visibility
for individual in range(1, 4):
    for time_of_day, color in colors.items():
        subset = result_data[(result_data['Individual'] == f'Person {individual}') & (result_data['TimeOfDay'] == time_of_day)]
        x = subset['Day']
        y = subset['Fatigue']
        
        # Scatter plot for actual data points
        plt.scatter(x, y, color=color, alpha=0.6, label=f'{individual} {time_of_day} Data')
        
        # Lowess smoother for trend
        lowess = sm.nonparametric.lowess(y, x, frac=0.3)
        plt.plot(lowess[:, 0], lowess[:, 1], color=color, label=f'{individual} {time_of_day} Trend')

plt.title('Detailed Fluctuations and Frequency of Fatigue Levels by Individual and Time of Day')
plt.xlabel('Day')
plt.ylabel('Fatigue Level')
plt.legend(title='Individual / Time of Day', bbox_to_anchor=(1.05, 1), loc='upper left')
plt.grid(True)
plt.show()
#----------------------------------------------------------------------------------------------------
# Redefining necessary imports and class to simulate data
import numpy as np
import math
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

class PVTModel:
    def __init__(self, factors):
        self.factors = factors  # Individual factors for morning, afternoon, and night
        self.std_dev = 0.05  # Standard deviation for reaction time
        self.days = 30
    
    def _fatigue(self, cap_factor: float, avg_util: float, man_asm: int):
        fat_acc = (1 / cap_factor) * (math.exp(avg_util * man_asm) - 1)
        return fat_acc
    
    def _response_time(self, fat_lvl: float, cap_factor: float, res_origin: float):
        mean = res_origin + ((1 / cap_factor) * math.log(fat_lvl + 1))
        rsp_pre = np.random.normal(mean, self.std_dev, 1).item()
        rsp = max(rsp_pre, 0)  # Ensure response time is not negative
        return rsp
    
    def simulate(self):
        results = pd.DataFrame(columns=['Individual', 'Day', 'TimeOfDay', 'ReactionTime', 'Fatigue'])
        time_of_day_modifiers = {
            'Morning': 1.0,
            'Afternoon': 0.9,
            'Night': 0.85
        }
        
        for individual, factors in enumerate(self.factors, start=1):
            for day in range(1, self.days + 1):
                for time_of_day, modifier in time_of_day_modifiers.items():
                    cap_factor = factors[time_of_day]
                    reaction_time_base = 0.2433 * modifier  # Base reaction time adjusted for time of day
                    fatigue = self._fatigue(cap_factor, reaction_time_base, 2)  # Arbitrary manual asm for example
                    reaction_time = self._response_time(fatigue, cap_factor, reaction_time_base)
                    
                    new_row = pd.DataFrame({
                        'Individual': [f'Person {individual}'],
                        'Day': [day],
                        'TimeOfDay': [time_of_day],
                        'ReactionTime': [reaction_time],
                        'Fatigue': [fatigue]
                    })
                    results = pd.concat([results, new_row], ignore_index=True)
        
        return results

# Factors for each individual and time of day
factors = [
    {'Morning': 1.0, 'Afternoon': 0.95, 'Night': 0.9},  # Individual 1
    {'Morning': 0.9, 'Afternoon': 0.85, 'Night': 0.8},  # Individual 2
    {'Morning': 0.85, 'Afternoon': 0.8, 'Night': 0.75}  # Individual 3
]

# Initialize and simulate
pvt_model = PVTModel(factors)
result_data = pvt_model.simulate()

# Plotting the fatigue levels for each individual specifically for morning, afternoon, and night
fig, axs = plt.subplots(3, 1, figsize=(14, 18), sharex=True)

# Morning fatigue plot for each individual
morning_data = result_data[result_data['TimeOfDay'] == 'Morning']
sns.lineplot(data=morning_data, x='Day', y='Fatigue', hue='Individual', ax=axs[0], marker='o')
axs[0].set_title('Morning Fatigue for Each Individual')
axs[0].set_ylabel('Fatigue Level')

# Afternoon fatigue plot for each individual
afternoon_data = result_data[result_data['TimeOfDay'] == 'Afternoon']
sns.lineplot(data=afternoon_data, x='Day', y='Fatigue', hue='Individual', ax=axs[1], marker='o')
axs[1].set_title('Afternoon Fatigue for Each Individual')
axs[1].set_ylabel('Fatigue Level')

# Night fatigue plot for each individual
night_data = result_data[result_data['TimeOfDay'] == 'Night']
sns.lineplot(data=night_data, x='Day', y='Fatigue', hue='Individual', ax=axs[2], marker='o')
axs[2].set_title('Night Fatigue for Each Individual')
axs[2].set_ylabel('Fatigue Level')

# Common settings
for ax in axs:
    ax.set_xlabel('Day')
    ax.grid(True)
    ax.legend(title='Individual')

plt.tight_layout()
plt.show()
