#-----------------------------------------------------------------------------
# File: 	    main.py
# 
# Description: 	The main executable for the human response time model simulation.
#
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
# Library Imports
#-----------------------------------------------------------------------------

import human_model as hml

#-----------------------------------------------------------------------------
# Initializations
#-----------------------------------------------------------------------------

rt_mean: float = 0.2433 #mean of response time
rt_stdv: float = 0.042 #standard deviation of response time
rt_mean_delta: float = 0.0007 #change in mean of response time
rt_stdv_delta: float = 0.0003 #change in standard deviation of response time
cap_factor: float = 2.00 #capability of subject
instances: int = 100 #number of iterations

#-----------------------------------------------------------------------------
# Main Procedure
#-----------------------------------------------------------------------------

hml.HumanRTModel(rt_mean, rt_stdv, rt_mean_delta, rt_stdv_delta, \
                       cap_factor, instances)

print("Simulation Complete.")

