# -*- coding: utf-8 -*-
"""
Created on Sun Jun  9 23:24:59 2024

@author: asm33v
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# Define the polynomial model function
def polynomial_model(x, a, b, c, d):
    return a + b*x + c*x**2 + d*x**3

# Data for each individual (transcribed from your provided data)
df120_diff = np.array([13, 24.6, 16, 9.6, 12.4, -5.6, -25.4, -19.4, -7.5])# -14.1, -15.7, -4.1, -4, 17.2])# 10.6, 10.4, -12.2, -6.4, -14.3])
df122_diff = np.array([-37.7, -2.2, 2.9, -35.2, 6.7, -3.2, 4.3, -31.1, -32.2])#, -19.3, 18.7, -3.4, -17.5, 28.5])# -1.1, -33.2, -14.1, -13.3, -0.2])
df116_diff = np.array([9, 90, -14.4, -31.1, -16.4, 5.9, -40.5, -10.7, -2.3])#, 34.4, -258.6, -61.4, -29.3, 50.5])# -32.4, 26.1, -14.3, -16.6, 14.4])
df128_diff = np.array([104.8, 11, -13.7, 85.3, -7.1, -121.5, -121.2, 10.6, 111.2])#, -50.9, 94.6, -48.9, 6, -8.6])
df131_diff = np.array([66.4, -4.6, -12.3, -2.7, -5.8, -6.3, 4.8, 3.1, -3.1])
df132_diff = np.array([-2.3, 89.5, 4.5, 140.2, 44.1, 62.1, -85.6, -6.2, 19.9])
df101_diff = np.array([-29.9, -3.9, -20.1, -15.4, -1.9, 170.8, -26.8, -9.6, -27.8])
df109_diff = np.array([-28.2, 3.4, 30, 15.1, 20.3, 10.5, 32.1, 3, 10.6])
df127_diff = np.array([51.2, 8.9, -14.3, 9.6, -26.6, -6.5, 34.1, 35.2, 4.7])
df125_diff = np.array([103.6, 54.4, 49.8, 24.9, 0, -19.5, 2, -7.7, 15.9])
df124_diff = np.array([29.4, -47.5, 23.1, 37, -44.4, 34.4, 34.9, -9.6, -20])
df121_diff = np.array([-25, 74.9, -12.8, -71, 0.8, 31, -33.9, -87.3, 21.8])
df110_diff = np.array([-6.1, -3, -23.1, 0.3, -34.8, -16.6, -9.4, -22, -29.2])

    

# Days array (assumes data points for all three datasets are of the same length)
days = np.arange(1, len(df120_diff) + 1)

# Perform curve fitting and plot for each dataset
params = {}
plt.figure(figsize=(10, 6))

for name, data in zip(['DF120', 'DF122', 'DF116', 'DF128', 'DF131', 'DF132', 'DF101', 'DF109', 'DF127', 'DF125', 
                       'DF124', 'DF121', 'DF110'], [df120_diff, df122_diff, df116_diff, df128_diff, df131_diff, 
                                                    df132_diff, df101_diff, df109_diff, df127_diff, df125_diff, 
                                                    df124_diff, df121_diff, df110_diff]):
    popt, pcov = curve_fit(polynomial_model, days, data, method='dogbox', maxfev=5000)
    params[name] = popt
    plt.scatter(days, data, label=f'Actual {name}')
    plt.plot(days, polynomial_model(days, *popt), label=f'Fitted {name}')

plt.title('Polynomial Curve Fit for Reaction Time Differences')
plt.xlabel('Day')
plt.ylabel('Reaction Time Difference (ms)')
plt.xticks(days)
#plt.legend()
plt.grid(True)
plt.show()

# Output the parameters
print("Fitted parameters for each individual:")
for name, popt in params.items():
    print(f"{name}: {popt}")
