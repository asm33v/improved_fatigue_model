# -*- coding: utf-8 -*-
"""
Created on Mon Jun 24 16:28:32 2024

@author: asm33v
"""

import pandas as pd

df = pd.read_csv("C:/Users/asm33v/Desktop/ERT-All.csv")
df = df[['Subject Id', 'Session start', 'Session end', 'Mean RT (ms)']]

def subjectid(x):
    return str(x)[4:]

def clean_datetime_string(dt_str):
    return dt_str.replace('T', 'T ').strip()

df['Subject Id'] = df['Subject Id'].apply(subjectid)
df['Session start'] = df['Session start'].apply(clean_datetime_string)
df['Session end'] = df['Session end'].apply(clean_datetime_string)
with pd.ExcelWriter("C:/Users/asm33v/Desktop/ert_separated_me.xlsx") as writer:
    df.to_excel(writer, index=False)
