# -*- coding: utf-8 -*-
"""
Created on Fri May 31 20:19:16 2024

@author: Anna
"""

import pandas as pd

# Load the data from the provided CSV file
file_path = r'C:/Users/asm33v/Desktop/pvt_combined_second_rows_time_corrected.csv'
data = pd.read_csv(file_path)

# Display the first few rows of the dataframe to understand its structure
print(data.head())
#-----------------------------------------------------------------
# For demonstration, we assume constants for α, β, M, and t as no specific values are given
alpha = 1.0  # Capability factor
beta = 0.1   # Average time on task per instance
M = 30       # Maximum task time per instance
t = 10       # Total tasks completed

# Convert session start time to datetime and extract hour for categorization
data['Session Start'] = pd.to_datetime(data['Session start'], utc=True)
data['Time of Day'] = data['Session Start'].dt.hour.apply(lambda x: 'Morning' if 8 <= x < 16 else 'Evening')

# Keeping the reaction time values
beta = data['Mean RT (ms)']
print(beta)
# Calculate the fatigue using the given formula
import numpy as np
data['Fatigue'] = (1 / alpha) * (np.exp(( beta[2] / M) * t) - 1)

# Select relevant columns to save in a new DataFrame
final_data = data[['Session Start', 'Time of Day', 'Mean RT (ms)', 'Fatigue']]

# Save the processed data to a new CSV file
output_file_path = 'C:/Users/asm33v/Desktop/fatigue_of_combined.csv'
final_data.to_csv(output_file_path, index=False)

output_file_path
#--------------------------------------------------------------------------------
import matplotlib.pyplot as plt

# Filter data for Morning and Evening
morning_data = final_data['Time of Day'] == 'Morning'
evening_data = final_data['Time of Day'] == 'Evening'
#print(morning_data)
# Plotting the fatigue for Morning and Evening sessions
plt.figure(figsize=(14, 6))

plt.subplot(1, 2, 1)
plt.plot(morning_data['Session start'], morning_data['Fatigue'], 'bo')
plt.title('Morning Sessions Fatigue')
plt.xlabel('Session Start Time')
plt.ylabel('Fatigue')
plt.xticks(rotation=45)

plt.subplot(1, 2, 2)
plt.plot(evening_data['Session start'], evening_data['Fatigue'], 'ro')
plt.title('Evening Sessions Fatigue')
plt.xlabel('Session Start Time')
plt.ylabel('Fatigue')
plt.xticks(rotation=45)

plt.tight_layout()
plt.show()
#--------------------------------------------------------------------
# Plotting with a suitable format using a smoother line style for better visualization
plt.figure(figsize=(12, 6))

# Plotting Morning and Evening sessions with a smoother line style
plt.plot(morning_data_sorted['Session start'], morning_data_sorted['Fatigue'], marker='o', linestyle='-', color='pink', label='Morning Fatigue')
plt.plot(evening_data_sorted['Session start'], evening_data_sorted['Fatigue'], marker='o', linestyle='-', color='green', label='Evening Fatigue')

plt.title('Combined Fatigue Trends for Morning and Evening Sessions')
plt.xlabel('Session Start Time')
plt.ylabel('Fatigue')
plt.legend()
plt.xticks(rotation=45)
plt.grid(True)
plt.tight_layout()
plt.show()
#----------------------------------------------------------------------------------
# Creating a histogram of the fatigue values for both morning and evening sessions combined
plt.figure(figsize=(10, 6))

# Histogram of fatigue values
plt.hist([morning_data_sorted['Fatigue'], evening_data_sorted['Fatigue']], bins=20, color=['pink', 'green'], label=['Morning Fatigue', 'Evening Fatigue'], alpha=0.75)
plt.title('Histogram of Fatigue Values')
plt.xlabel('Fatigue')
plt.ylabel('Frequency')
plt.legend()
plt.grid(True)
plt.show()
#------------------------------------------------------------------------
# Redefine the forecast index for evening data
evening_forecast_index = pd.date_range(evening_diff.index[-1], periods=11, freq='D')[1:]

# Redraw the plots with the correct indices and without using confidence intervals
plt.figure(figsize=(14, 7))

# Morning fatigue plot with forecast
plt.subplot(1, 2, 1)
plt.plot(morning_resampled.index, morning_resampled['Fatigue'], label='Observed Morning Fatigue', color='pink')
plt.plot(pd.date_range(morning_resampled.index[-1], periods=11, freq='D')[1:], forecast_morning.predicted_mean, label='Forecasted Morning Fatigue', color='red')
plt.title('Morning Fatigue and Forecast')
plt.xlabel('Date')
plt.ylabel('Fatigue')
plt.legend()

# Evening fatigue plot with forecast
plt.subplot(1, 2, 2)
plt.plot(evening_diff.index, evening_diff, label='Differenced Evening Fatigue', color='green')
plt.plot(evening_forecast_index, forecast_evening.predicted_mean, label='Forecasted Evening Fatigue', color='darkgreen')
plt.title('Evening Fatigue and Forecast')
plt.xlabel('Date')
plt.ylabel('Fatigue')
plt.legend()

plt.tight_layout()
plt.show()
#----------------------------------------------------------------------
