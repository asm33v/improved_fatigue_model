# -*- coding: utf-8 -*-
"""
Created on Tue Jun 11 22:36:56 2024

@author: asm33v
"""

import numpy as np
import pandas as pd
import statsmodels.api as sm
import matplotlib.pyplot as plt

# Example predictor variable (if applicable, like time of day coded numerically or another continuous or categorical variable)
x = np.linspace(1, 9, 9)  # assuming there are 9 measurements per individual corresponding to 9 different times or conditions

# Assuming constant term is needed for the OLS model
x = sm.add_constant(x)

# Sample data setup
data = {
    'DF120': np.array([13, 24.6, 16, 9.6, 12.4, -5.6, -25.4, -19.4, -7.5]),
    'DF122': np.array([-37.7, -2.2, 2.9, -35.2, 6.7, -3.2, 4.3, -31.1, -32.2]),
    'DF116': np.array([9, 90, -14.4, -31.1, -16.4, 5.9, -40.5, -10.7, -2.3]),
    'DF128': np.array([104.8, 11, -13.7, 85.3, -7.1, -121.5, -121.2, 10.6, 111.2]),
    'DF131': np.array([66.4, -4.6, -12.3, -2.7, -5.8, -6.3, 4.8, 3.1, -3.1]),
    'DF132': np.array([-2.3, 89.5, 4.5, 140.2, 44.1, 62.1, -85.6, -6.2, 19.9]),
    'DF101': np.array([-29.9, -3.9, -20.1, -15.4, -1.9, 170.8, -26.8, -9.6, -27.8]),
    'DF109': np.array([-28.2, 3.4, 30, 15.1, 20.3, 10.5, 32.1, 3, 10.6]),
    'DF127': np.array([51.2, 8.9, -14.3, 9.6, -26.6, -6.5, 34.1, 35.2, 4.7]),
    'DF125': np.array([103.6, 54.4, 49.8, 24.9, 0, -19.5, 2, -7.7, 15.9]),
    'DF124': np.array([29.4, -47.5, 23.1, 37, -44.4, 34.4, 34.9, -9.6, -20]),
    'DF121': np.array([-25, 74.9, -12.8, -71, 0.8, 31, -33.9, -87.3, 21.8]),
    'DF110': np.array([-6.1, -3, -23.1, 0.3, -34.8, -16.6, -9.4, -22, -29.2])
    # Add other individuals similarly...
}

# Prepare a DataFrame for easy handling and visualization
df = pd.DataFrame(data)

results = {}

for key, values in data.items():
    model = sm.OLS(values, x)
    results[key] = model.fit()

# Print the summary for each individual
for key, result in results.items():
    print(f"Results for {key}:")
    print(result.summary())
    print("\n")

fig, axes = plt.subplots(nrows=3, ncols=3, figsize=(15, 15))  # Adjust the grid size based on the number of individuals
axes = axes.flatten()

for idx, key in enumerate(data.keys()):
    axes[idx].scatter(x[:, 1], data[key], label=f'Actual {key}')
    axes[idx].plot(x[:, 1], results[key].predict(x), color='red', label=f'Fitted {key}')
    axes[idx].set_title(key)
    axes[idx].legend()

plt.tight_layout()
plt.show()
